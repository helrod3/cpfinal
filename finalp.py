"""
Fake Stereo: Make an Anaglyph Image from an image and mask

Hal Elrod - CP Summer 2016
."""

import numpy as np
import cv2

import blend

# interesting, but not used: https://github.com/miguelgrinberg/anaglyph.py

def make_anaglyph(img_left, img_right):
    """Create a red/cyan anaglyph image using grayscale left and right images."""
    return np.dstack([img_right, img_right, img_left])

def show(img_left, img_right):
    cv2.imshow("Left image", img_left)
    cv2.imshow("Right image", img_right)

    img_ana = make_anaglyph(img_left, img_right)
    cv2.imshow("Anaglyph image", img_ana)

def translate(image, mask):
    mask[mask > 253] = 255
    mask[mask < 255] = 0

    object3d = np.copy(image)
    holeImage = np.copy(image)
    object3d[mask > 254] = 255

    #object3d = cv2.copyMakeBorder(object3d, 0, 0, 20, 0, cv2.BORDER_REFLECT101)
    #cv2.imshow('object3D', object3d)
    cv2.imshow('mask', mask)

    M = np.float32([[1,0,-10], [0,1,0]])

    newmask = cv2.warpAffine(object3d, M, (mask.shape[1], mask.shape[0]))
    cv2.imshow('newmask', newmask)
    newmask = newmask.astype(float)
    mask = mask.astype(float) / 255

    blended = blend.run_blend(black_image=newmask, white_image=image, mask=mask)
    #blended = image + newmask
    #cv2.imshow('move', move)
    return blended.astype(np.uint8)


def stereophy(image, mask):
    xmap = np.zeros((mask.shape), dtype=np.float32)
    ymap = np.zeros((mask.shape), dtype=np.float32)

    # invert mask -- dark = further = less movement
    mask = 255 - mask

    parallax_factor = 10
    for r in range(mask.shape[0] - 1):
        for c in range(mask.shape[1]):
            xmap[r, c] = c - (mask[r,c] / parallax_factor)
            ymap[r, c] = r

    cv2.imshow('mask', mask)
    newimage = cv2.remap(image, xmap, ymap, interpolation=cv2.INTER_LANCZOS4)
    newimage = cv2.GaussianBlur(newimage, (5,5), 0)

    cv2.imshow('remapped', newimage)
    return newimage

if __name__ == "__main__":
    import errno
    import os

    image_dir = os.path.join("images", "in")
    out_dir = os.path.join("images", "out")

    print "Reading images."
    img_left = cv2.imread("images\in\image4.jpg", cv2.IMREAD_GRAYSCALE)  # grayscale
    #img_right = cv2.imread("images\in\image-right.jpg", cv2.IMREAD_GRAYSCALE)  # grayscale
    img_mask = cv2.imread("images\in\mask4.jpg", cv2.IMREAD_GRAYSCALE)
    img_left = cv2.flip(img_left, 1)
    img_mask = cv2.flip(img_mask, 1)
    new_right = stereophy(img_left, img_mask)
    print "Make Analglyph"

    show(img_left, new_right)
    #show(img_left, img_right)
    cv2.waitKey()




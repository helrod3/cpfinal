import numpy as np
import scipy.signal
import math

# Hal C Elrod
# Summer 2016

""" Blending

Modified from my Assignment 6 code

"""

def generatingKernel(parameter):
    """ Return a 5x5 generating kernel based on an input parameter.

    Note: This function is provided for you, do not change it.

    Args:
      parameter (float): Range of value: [0, 1].

    Returns:
      numpy.ndarray: A 5x5

    """
    kernel = np.array([0.25 - parameter / 2.0, 0.25, parameter,
                       0.25, 0.25 - parameter /2.0])
    return np.outer(kernel, kernel)

def reduce(image):
    """ Convolve the input image with a generating kernel of parameter of 0.4 and
    then reduce its width and height by two.

    Please consult the lectures and readme for a more in-depth discussion of how
    to tackle the reduce function.

    For grading purposes, it is important that you use a zero border in the convolution
    and that you include the first row (column), skip the second, etc in the
    sampling phase.  You can use any / all functions to accomplish these ends.

    Args:
      image (numpy.ndarray): a grayscale image of shape (r, c)

    Returns:
      output (numpy.ndarray): an image of shape (ceil(r/2), ceil(c/2))
        For instance, if the input is 5x7, the output will be 3x4.
        The dtype should be 64-bit floats.

    """
    # WRITE YOUR CODE HERE.

    kernel = generatingKernel(0.4)

    # first convolve image. Default is zero fill
    gimage = scipy.signal.convolve2d(image, kernel, 'same')

    #take every other row, column
    newimage = gimage[0::2, 0::2]


    return newimage

    # END OF FUNCTION.

def expand(image):
    """ Expand the image to double the size and then convolve it with a generating
    kernel with a parameter of 0.4.

    Args:
      image (numpy.ndarray): a grayscale image of shape (r, c)

    Returns:
      output (numpy.ndarray): an image of shape (2*r, 2*c)
        The dtype should be 64-bit floats.
    """
    # WRITE YOUR CODE HERE.

    # http://stackoverflow.com/questions/9793731/how-to-replace-every-n-th-value-of-an-array-in-python-most-efficiently
    # default zeros datatype is np.float64

    newimage = np.zeros((image.shape[0] * 2, image.shape[1] * 2))
    kernel = generatingKernel(0.4)

    # woot!
    newimage[0::2, 0::2] = image

    # there may be some slicing way to do this...but this works fine
    # for rows in range(image.shape[0]):
    #   for cols in range(image.shape[1]):
    #     newimage[(rows * 2)][(cols * 2)] = image[rows][cols]

    gimage = scipy.signal.convolve2d(newimage, kernel, 'same')

    return gimage * 4

    # END OF FUNCTION.

def gaussPyramid(image, levels):
    """ Construct a pyramid from the image by reducing it by the number of levels
    passed in by the input.

    Note: You need to use your reduce function in this function to generate the
    output.

    Args:
      image (numpy.ndarray): an image of dimension (r,c) and dtype float.
      levels (uint8): a positive integer that specifies the number of reductions
                      you should do. So, if levels = 0, you should return a list
                      containing just the input image. If levels = 1, you should
                      do one reduction. len(output) = levels + 1

    Returns:
      output (list): A list of arrays of dtype np.float. The first element of the
                     list (output[0]) is layer 0 of the pyramid (the image
                     itself). output[1] is layer 1 of the pyramid (image reduced
                     once), etc. We have already included the original image in
                     the output array for you. The arrays are of type
                     numpy.ndarray.

    Consult the lecture and README for more details about Gaussian Pyramids.
    """
    output = [image]
    # WRITE YOUR CODE HERE.

    while levels > 0:
        newimage = reduce(image)
        output.append(newimage)
        image = newimage
        levels -= 1

    return output

    # END OF FUNCTION.

def laplPyramid(gaussPyr):
    """ Construct a laplacian pyramid from the gaussian pyramid, of height levels.

    Note: You must use your expand function in this function to generate the
    output. The Gaussian Pyramid that is passed in is the output of your
    gaussPyramid function.

    Args:
      gaussPyr (list): A Gaussian Pyramid as returned by your gaussPyramid
                       function. It is a list of numpy.ndarray items.

    Returns:
      output (list): A laplacian pyramid of the same size as gaussPyr. This
                     pyramid should be represented in the same way as guassPyr,
                     as a list of arrays. Every element of the list now
                     corresponds to a layer of the laplacian pyramid, containing
                     the difference between two layers of the gaussian pyramid.

             output[k] = gaussPyr[k] - expand(gaussPyr[k + 1])

             Note: The last element of output should be identical to the last
             layer of the input pyramid since it cannot be subtracted anymore.

    """
    output = []
    # WRITE YOUR CODE HERE.

    maxlayer = gaussPyr.__len__() - 1
    for k in range(maxlayer):
        ep = expand(gaussPyr[k + 1])
        lp = gaussPyr[k] - ep[:gaussPyr[k].shape[0], 0:gaussPyr[k].shape[1]]
        output.append(lp)

    output.append(gaussPyr[maxlayer])
    return output

    # END OF FUNCTION.

def blend(laplPyrWhite, laplPyrBlack, gaussPyrMask):
    """ Blend the two laplacian pyramids by weighting them according to the
    gaussian mask.

    Args:
      laplPyrWhite (list): A laplacian pyramid of one image, as constructed by
                           your laplPyramid function.

      laplPyrBlack (list): A laplacian pyramid of another image, as constructed by
                           your laplPyramid function.

      gaussPyrMask (list): A gaussian pyramid of the mask. Each value is in the
                           range of [0, 1].


    """

    blendedPyr = []
    # Insert your code here ------------------------------------------------------

    maxlayer = laplPyrWhite.__len__()
    for k in range(maxlayer):
        newblend = (gaussPyrMask[k] * laplPyrWhite[k]) + ((1 - gaussPyrMask[k]) * laplPyrBlack[k])
        blendedPyr.append(newblend)

    return blendedPyr


    # ----------------------------------------------------------------------------
    return blendedPyr

def collapse(pyramid):
    """ Collapse an input pyramid.

    Args:
    pyramid (list): A list of numpy.ndarray images. You can assume the input is
                    taken from blend() or laplPyramid().

    Returns:
      output(numpy.ndarray): An image of the same shape as the base layer of the
                             pyramid and dtype float.


    """
    # Insert your code here ------------------------------------------------------

    maxlayer = pyramid.__len__()
    lastimg = pyramid[maxlayer - 1]

    for image in pyramid[maxlayer - 2::-1]:
        smaller = expand(lastimg)
        newblend = image + smaller[:image.shape[0], 0:image.shape[1]]
        lastimg = newblend

    return newblend

    # ----------------------------------------------------------------------------


# adapted from assignment6_test.py
def run_blend(black_image, white_image, mask):
  """ This function administrates the blending of the two images according to
  mask.

  Assume all images are float dtype, and return a float dtype.
  """

  # Automatically figure out the size
  min_size = min(black_image.shape)
  depth = int(math.floor(math.log(min_size, 2))) - 4 # at least 16x16 at the highest level.

  gauss_pyr_mask = gaussPyramid(mask, depth)
  gauss_pyr_black = gaussPyramid(black_image, depth)
  gauss_pyr_white = gaussPyramid(white_image, depth)


  lapl_pyr_black  = laplPyramid(gauss_pyr_black)
  lapl_pyr_white = laplPyramid(gauss_pyr_white)

  outpyr = blend(lapl_pyr_white, lapl_pyr_black, gauss_pyr_mask)
  outimg = collapse(outpyr)

  outimg[outimg < 0] = 0 # blending sometimes results in slightly out of bound numbers.
  outimg[outimg > 255] = 255
  outimg = outimg.astype(np.uint8)

  return outimg

